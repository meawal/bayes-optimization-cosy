import numpy as np
from ibl_lib import log
from ibl_lib.bayes import BayesianOptimization, UtilityFunction, SequentialDomainReductionTransformer
import threading, time

class Acquisition(UtilityFunction):
    def __init__(self, kind='ei', epsilon=0, decay=1):
        assert kind in ['ucb', 'ei', 'poi']
        if kind == 'ucb':
            super().__init__(kind='ucb', xi=0, kappa=epsilon, kappa_decay=decay)
        else:
            super().__init__(kind=kind, xi=epsilon, kappa=0)
        

class StepBayesianOptimizer(BayesianOptimization):
    def __init__(self, space_bounds, acq='ei', epsilon=0, decay=1, alpha=1e-2, eta=0.8, gamma = 32, 
                 reduce_space=True, random_state=None):
        self._space_bounds = space_bounds
        self._reduce_space = reduce_space
        bounds_transformer = SequentialDomainReductionTransformer(gamma_osc = gamma, 
                                                                  gamma_pan = 1.0,
                                                                  eta = eta)
        super().__init__(f=None, pbounds=self._space_bounds, random_state=random_state, alpha=alpha, 
                         verbose=0, bounds_transformer=bounds_transformer)
        self.eta = eta
        self.gamma = gamma
        self.acq = Acquisition(acq, epsilon, decay)
        self.max_space_bound = np.max(self._space.bounds[:,1] - self._space.bounds[:,0])
        self.ROUNDING = 2
        self.attempts_limit = 3
        self.convergence_bound_limit = .2
        
    @property
    def eta(self):
        return self._bounds_transformer.eta

    @eta.setter
    def eta(self, value):
        log.info(f'eta is set to {value}')
        self._bounds_transformer.eta = value

    @property
    def gamma(self):
        return self._bounds_transformer.gamma_osc

    @gamma.setter
    def gamma(self, value):
        log.info(f'gamma is set to {value}')
        self._bounds_transformer.gamma_osc = value

    @property
    def reduce_space(self):
        return self._reduce_space

    @reduce_space.setter
    def reduce_space(self, value):
        log.info(f'reduce space is set to {str(value)}')
        self._reduce_space = value

    def reset_space_bound(self):
        self.set_bounds(self._space_bounds)
        self._bounds_transformer = SequentialDomainReductionTransformer(
            gamma_osc = self.gamma, gamma_pan = 1.0, eta = self.eta)
        self._bounds_transformer.initialize(self._space)
    
    def suggest(self):
        '''Return the next point with most potential with respect to the acquisition function.'''
        self.max_space_bound = np.max(self._space.bounds[:,1] - self._space.bounds[:,0])
        for _ in range(self.attempts_limit):
            # get the next acquisition point
            point = super().suggest(self.acq)
            # round the values
            for key in point:
                point[key] = np.round(point[key], self.ROUNDING)
            # verify the point doesn't exist in the space
            if self._space._as_array(point) not in self._space:
                return point, self.max_space_bound < self.convergence_bound_limit
        else: # BO has converged
            return point, True
        
    def register(self, point, value):
        '''Add an observation (point + value) to the space of the GP model.'''
        super().register(params=point, target=value)
        if self.reduce_space:
            self.set_bounds(self._bounds_transformer.transform(self._space))
        
    @property
    def epsilon(self):
        return self.acq.xi
    
    @epsilon.setter
    def epsilon(self, value):
        self.acq.xi = value
        
    @property
    def alpha(self):
        return self._gp.alpha
    
    @alpha.setter
    def alpha(self, value):
        self._gp.alpha = value
