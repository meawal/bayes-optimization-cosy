import numpy as np
from ibl_lib import IBLEpics, log
import pandas as pd
pd.options.display.max_rows = 100
pd.options.display.float_format = '{:,.2f}'.format

class IBLSection(IBLEpics):
    def __init__(self, start_component='BC11', target_component='TUM', components='ibl-components.csv', 
                 init_target=None):
        super().__init__(components=components, init_target=init_target)
        assert (start_component in self.components.index) \
            and (target_component in self.components.index), \
            f'{start_component} or {target_component} not in the list of componenets'
        self.start_s = self.components.loc[start_component].s
        self.end_s = self.components.loc[target_component].s
        assert self.end_s >= self.start_s, 'target must be after the start component'
        between = (self.components.s >= self.start_s) & (self.components.s <= self.end_s)
        self.components = self.components[between].copy()
        self._magnets = None
        self._beamcups = None
        self._target_comp = self.components.loc[target_component].copy()
        self._target_comp['value'] = None
        
