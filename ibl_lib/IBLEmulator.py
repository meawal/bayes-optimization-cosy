import os
import numpy as np
from cpymad.madx import Madx
import pandas as pd
from collections import OrderedDict
from ibl_lib import log, IBL

class IBLEmulator(IBL):
    '''MAD-X simulation for the Injection Beam Line (IBL)'''
    def __init__(self, comp_file='ibl-components.csv', call_file='ibl.madx', 
                 mag_file='ibl_mag_set.txt', directory='ibl_madx'):
        '''Initialize the object.'''
        log.info(f'IBLMadx mudule is being used on the directory {directory}')
        self.call_file = call_file
        self.mag_file = mag_file
        self.ptc_file = ptc_file
        self.directory = directory
        
        self.twiss = None
        self.length = None
        
        self.madx = Madx(stdout=False)
        self.madx.verbose(False)
        self.madx.chdir(self.directory)
        self.read_magnets_state()
        
    def read_magnets(self):
        raise NotImplementedError()
        
    def set_magnets(self, value_dict):
        raise NotImplementedError()
        
    @property
    def magnets(self):
        raise NotImplementedError()
    
    @property
    def beamcups(self):
        raise NotImplementedError()
    
    @property
    def target_bc(self):
        raise NotImplementedError()
    
    @property
    def value_bounds(self):
        raise NotImplementedError()
        
