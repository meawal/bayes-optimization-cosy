import pandas as pd
import numpy as np
from collections import OrderedDict
from ibl_lib import log, IBL, utils
import epics
import time
from wurlitzer import pipes

class IBLEpics(IBL):
    def __init__(self, components='ibl-components.csv', init_target=None):
        super().__init__(components=components)
        self.init_target = init_target
        self._et = None
        self._ev = None
        self._em = None

    def read_target_value(self, averaging_cycles=1):
        assert averaging_cycles >= 1
        mvalues = []
        sign = -1 if self.target_comp.type == 'beam_cup' else 1
        
        if self.target_comp.type == 'bct':
            while mvalues == []:
                cycle_length_pv = f'{utils.PREFIX}CTRL:CYCLE:LENGTH'
                cycle_length = self._read_pvs([cycle_length_pv])[0]
                sec = cycle_length * averaging_cycles
                bct_pv = f'{utils.PREFIX}{self.target_comp.pv_name}:{self.target_comp.pv_read}'
                log.debug('setting camonitor')
                epics.camonitor(bct_pv, writer=mvalues.append)
                time.sleep(sec)
                epics.camonitor_clear(bct_pv)
            values = [i.split(' ')[-1] for i in mvalues]
            temp = []
            for i in values:
                v = np.float64(i)
                if np.isfinite(v):
                    temp.append(v/1e8)
            if not np.isfinite(np.mean(temp)):
                log.info('non finite mean')
                self._et = temp
                self._ev = values
                self._em = mvalues

            return np.mean(temp)
        else:
            values=[]
            for i in range(averaging_cycles):
                # compute and read the target PV
                bc_current = f'{utils.PREFIX}{self.target_comp.pv_name}:{self.target_comp.pv_read}'
                value = sign * self._read_pvs([bc_current])[0]
                values.append(value)
                self.wait_next_beam(skip=0)
            return np.mean(values)
        
    def read_target_NP(self, averaging_cycles=1):
        assert averaging_cycles >= 1
        values = []
        for i in range(averaging_cycles):
            # compute the number of particles at the target BC
            e = 1.602176634e-19
            bc_current = f'{utils.PREFIX}{self.target_comp.pv_name}:{self.target_comp.pv_read}'
            bc_scale = f'{utils.PREFIX}{self.target_comp.pv_name}'
            bc_scale = bc_scale[:-1] + 'Scale' + bc_scale[-1:]
            bc_sample = f'{utils.PREFIX}IBL:DBC:SampleTime_RBV'
            l_pv = [bc_current, bc_scale, bc_sample]

            (current_rb, scale_rb, sample_time_rb) = self._read_pvs([bc_current, bc_scale, bc_sample])
            count = (np.abs(current_rb)/scale_rb) * (sample_time_rb/e)
            values.append(count)
            self.wait_next_beam(skip=0)
        return np.mean(values)

    @property
    def is_magnets_ready(self):
        # declare the list of PVs to check
        l_pv = []
        for mag, row in self.magnets.iterrows():
            # skip the magnets with no implementation of READY PV like SH83A
            if not isinstance(row.pv_ready, str):
                continue
            pv = f'{utils.PREFIX}{row.pv_name}:{row.pv_ready}'
            l_pv.append(pv)
            
        # read the PVs
        values = self._read_pvs(l_pv)
        
        # return True if all are ready (==0)
        return (np.asarray(values)==0).all()
        
    def wait_next_beam(self, skip=0):
        cycle_time_pv = f'{utils.PREFIX}CTRL:CYCLE:T:10HZ'
        cycle_length_pv = f'{utils.PREFIX}CTRL:CYCLE:LENGTH'
        sample_time_pv = f'{utils.PREFIX}IBL:DBC:AveragingTime_RBV'
        
        cycle_time, cycle_length, sample_time = self._read_pvs([
            cycle_time_pv, cycle_length_pv, sample_time_pv])
        # wait till next beam + beam cycle skips + 50ms
        wait_time = cycle_length - cycle_time + sample_time + skip*cycle_length + .05
        time.sleep(wait_time)
        
    def read_magnets(self, update=True):
        '''Read the values of all Quadrupole and Steerer magnets. On failed attempts to read
        the PVs re-read is executed after waiting a Fibonacci time amount.
        '''
        # declare the list of PVs to be read
        l_pv = []
        for mag, row in self.magnets.iterrows():
            pv = f'{utils.PREFIX}{row.pv_name}:{row.pv_read}'
            l_pv.append(pv)
        
        # read the magnets values
        values = self._read_pvs(l_pv)
        # update the current values if update=True
        if update:
            self.magnets.value = values
        return pd.Series(data=values, index=self.magnets.index, name='value').to_dict()
        
    def read_all_magnets(self, typ = 'df'):
        '''Read the values of all Quadrupole and Steerer magnets. On failed attempts to read
        the PVs re-read is executed after waiting a Fibonacci time amount.
        '''
        assert typ in ['df', 'dict', 'list']
        # declare the list of PVs to be read
        l_pv = []
        magnets = self.components[
                self.components.type.isin(['quadrupole', 'h_steerer', 'v_steerer'])
            ].drop('s', axis=1).copy()
        for mag, row in magnets.iterrows():
            pv = f'{utils.PREFIX}{row.pv_name}:{row.pv_read}'
            l_pv.append(pv)
        
        # read the magnets values
        values = self._read_pvs(l_pv)
        magnets['value'] = values
        if typ == 'dict':
            return magnets.value.to_dict()
        elif typ == 'list':
            return magnets.value.tolist()
        else:
            return magnets
        
    def _read_pvs(self, pvs):
        # read the PVs
        values = epics.caget_many(pvs)
        
        # if failed while reading, display failed PVs, wait and reattempt reading
        attempt = 0
        while (None in values) and (attempt < len(utils.fibonacci)):
            # get the list of failed PVs
            l_fail = []
            for i in range(len(values)):
                if values[i] is None:
                    l_fail.append(pvs[i])
                    
            # log failed PVs
            log.warning(f'failed reading PVs: {l_fail}')
            
            # wait and increase the attempt count
            time.sleep(utils.fibonacci[attempt])
            attempt += 1
            
            # read the PVs
            log.info(f're-trying no. {attempt} to read the PVs')
            values = epics.caget_many(pvs)
            
        # if still some PVs are not readable raise exception
        if None in values:
            log.error(f'some PVs are not readable {l_fail}')
            raise Exception('failed at reading PVs')
            
        # otherwise return the values
        return values
    
    def set_magnets(self, value_dict):
        '''Set the current of the provided set of Quadrupole and Steerer magnets.
        '''
        # if the keys provided were for the PVs then just use them
        if 'IBL:' in value_dict:
            l_pv = list(value_dict.keys())
        else: # keys are the names of magnets and PVs should be computed
            l_pv=[]
            for key in value_dict.keys():
                # assert the provided name is trainable magnet
                assert key in self.magnets.index, f'"{key}" is not a trainable magnet'
                # get the magnet
                magnet = self.magnets.loc[key]
                # compute the PV name
                l_pv.append(f'{utils.PREFIX}{magnet.pv_name}:{magnet.pv_set}')
                
        # read the PV values
        l_value = list(value_dict.values())
        
        # set the values of the magnets and wait till completed
        limit = 60
        log.debug(f'attempt to set the currents of {len(l_pv)} magnets')
        epics.caput_many(pvlist=l_pv, values=l_value, wait='all', put_timeout=limit)
        wait = .5
        time.sleep(wait)
        for i in range(int(limit/wait)):
            if self.is_magnets_ready:
                log.debug(f'magnets are ready')
                break
            time.sleep(wait)
        else:
            log.warning('timed out while waiting for the magnets to be ready')
    
    @property
    def value_bounds(self):
        self.read_magnets()
        bounds = OrderedDict()
        for magnet, value in zip(self.magnets.index, self.magnets.value):
            if value > 0:
                bounds[magnet] = (1, 68)
            else:
                bounds[magnet] = (-68, 1)
            
        return bounds
