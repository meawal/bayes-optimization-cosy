import os
import numpy as np
from cpymad.madx import Madx
import pandas as pd
from collections import OrderedDict
import matplotlib.pyplot as plt
from wurlitzer import pipes

class PTC:
    def __init__(self, madx_table, length):
        '''Analyse the PTC track data from a MAD-X simulation for the IBL.
        '''
        # extract the ptc start config
        ptc_track = madx_table.tracksumm.dframe()
        self.start = ptc_track[ptc_track.turn==0].copy()
        self.start.reset_index(drop=True, inplace=True)
        self.length = length
        
        # compute the particles count
        self.count = self.start.shape[0]
        
        # compute the ptc tracking
        # get the data of not passed particles
        self.track = ptc_track[ptc_track.turn==1].copy()
        # get the passed particles
        pass_track = madx_table.trackone.dframe()
        pass_track = pass_track[(pass_track.index=='#e') & (pass_track.turn == 1)]
        l_pass = pass_track.number.tolist()
        # compute s in m and percentage for all particles
        s = self.track.apply(lambda row: row.s if row.number not in l_pass else self.length, 
                             axis=1)
        s_per = s/self.length
        # save the computed s and s_per
        self.track.s = s
        self.track.insert(self.track.shape[1]-1, 's_per', s_per)
        self.track.reset_index(drop=True, inplace=True)
        
        # compute the passed particles
        self.passed_count = self.track[self.track.s_per==1].shape[0]
        
    def plot_ptc_loss(self, norm=False, cummulative=False, ylim=None):
        self.track.s[self.track.s_per < 1].hist(
            bins=47, range=(0,self.length), figsize=(10,4), 
            density=norm, cumulative=cummulative)
        plt.xticks(np.arange(0,self.length, 10))
        plt.title('Lossed particles over IBL')
        plt.xlabel('s (m)')
        plt.ylabel('density' if norm else 'count')
        if ylim:
            plt.ylim(0, ylim)
        plt.show()
        
class IBLMadX:
    '''MAD-X simulation for the Injection Beam Line (IBL)'''
    def __init__(self, call_file='ibl.madx', mag_file='ibl_mag_set.txt', ptc_file = 'ptc_particles.txt',
                 directory='madx_ibl'):
        '''Initialize the object.'''
        self.call_file = call_file
        self.mag_file = mag_file
        self.ptc_file = ptc_file
        self.directory = directory
        
        self.twiss = None
        self.length = None
        
        with pipes():
            self.madx = Madx(stdout=False)
            self.madx.verbose(False)
            self.madx.chdir(self.directory)
        self.read_magnets()
                
    def run(self, debug=False):
        '''Run the IBL MAD-X simulation a single time and store the generated data.'''
        if debug:
            with pipes() as (out, err):
                self.madx = Madx(stdout=False)
                self.madx.verbose(False)
                self.madx.chdir(self.directory)
                self.madx.call(file=self.call_file)
            out = out.read()
            err = err.read()
            if len(out) > 0:
                print(f'Mad-X stdout: {out}')
            if len(err) > 0:
                print(f'Mad-X stderr: {err}')
        else:
            self.madx.call(file=self.call_file)
        
        self.length = self.madx.sequence['ibl'].length
        
        # check if the run is successful
        if self.madx.table.summ.dframe().shape[0]>0:
            # summary of the simulation runs
            self.summ = self.madx.table.summ.dframe().iloc[0]
            # extract the selected twiss values
            self.twiss = self.madx.table.twiss.selection().dframe()
        else:
            self.summ = None
            self.twiss = None
        
        # analyse PTC tracking data
        # self.ptc = PTC(self.madx.table, self.length)
        
    def generate_ptc(self, n):
        '''Generate random spaced PTC particles.
        
        Args:
        -----
        n: the number of particles to be generated.
        '''
        epsilon_x = 5.68e-6
        epsilon_y = 1.04e-5
        beta_x    = 1.18
        beta_y    = 1.98
        alpha_x   =-2.45e-1
        alpha_y   = 1.76


        jx = np.sqrt(beta_x*epsilon_x) * np.random.normal(0, 1, n)
        jx2= np.sqrt(beta_x*epsilon_x) * np.random.normal(0, 1, n)

        jy = np.sqrt(beta_y*epsilon_y) * np.random.normal(0, 1, n)
        jy2= np.sqrt(beta_y*epsilon_y) * np.random.normal(0, 1, n)

        x = np.round(jx, 10)
        px= np.round((jx2-alpha_x*jx)/beta_x, 10)
        y = np.round(jy, 10)
        py= np.round((jy2-alpha_y*jy)/beta_y, 10)

        file = os.path.join(self.directory, self.ptc_file)
        print(f"output file '{file}'")
        with open(file, 'w') as f_ptc:
            for i in range(n):
                f_ptc.write(f'ptc_start, x={x[i]},px={px[i]},y={y[i]},py={py[i]};\n')
                
    def read_magnets(self):
        '''Read the magnet settings.
        '''
        import re
        
        l_rows=[]
        
        # read the magnet settings file
        with open(os.path.join(self.directory, self.mag_file), 'r') as f_mag:
            data= f_mag.read().split('\n')
            
        # strip the file from white spaces and remove empty lines
        for i in range(len(data)):
            data[i]=data[i].strip()
        while '' in data:
            data.remove('')
            
        # extract the data as regex
        mag_re = r'(?P<name>\w+)\s*:?=\s*(?P<value>[-+]?\d*.?\d+)\s*;'
        type_dic = {'fQ':'quadrupole', 'hkick':'h.kicker', 'vkick':'v.kicker'}
        for mag in data:
            match = re.search(mag_re, mag).groupdict()
            magnet = match['name']
            value = float(match['value'])
            
            for key in type_dic:
                if key in magnet:
                    mag_type = type_dic[key]
                    break
            else:
                mag_type = 'unrecongnized'
                
            # add the extracted data as a row
            l_rows.append([magnet, value, mag_type])
                
        # buld the magnets data table
        menu = ['magnet', 'value', 'type']
        self.magnets = pd.DataFrame(columns=menu, data=l_rows).set_index('magnet')
        
    def save_magnets(self):
        data = []
        line = '{magnet:11s}={value:6.2f};\n'
        
        state = self.magnets.iloc[0].type
        for idx, row in self.magnets.iterrows():
            if state != row.type:
                data.append('\n')
                state = row.type
            data.append(line.format(magnet=idx, value=row.value))
            
        with open(os.path.join(self.directory, self.mag_file), 'w') as f_mag:
            f_mag.writelines(data)
            
    def plot_bet(self, ylim=None):
        if self.twiss is not None:
            plt.figure(figsize=(10,4))
            plt.plot(self.twiss.s.tolist(), self.twiss.betx.tolist(), label='bet x')
            plt.plot(self.twiss.s.tolist(), self.twiss.bety.tolist(), label='bet y')
            plt.xticks(np.arange(0,self.length, 10))
            plt.title(r'IBL swiss $\beta_x$ & $\beta_y$')
            plt.xlabel('s (m)')
            plt.ylabel(r'$\beta$ (m)')
            if ylim:
                plt.ylim(0, ylim)
            plt.legend()
            plt.grid()
            plt.show()
            
    @property
    def magnets_state(self):
        return self.magnets.value.to_dict(OrderedDict)
    
    @magnets_state.setter
    def magnets_state(self, value_dict):
        for key, value in value_dict.items():
            loc = self.magnets[self.magnets.index.str.lower()==key.lower()]
            assert loc.shape[0] > 0, f'magnet {key} is invalid'
            mag = loc.iloc[0].name
            self.magnets.at[mag, 'value'] = value
            
        self.save_magnets()
        
