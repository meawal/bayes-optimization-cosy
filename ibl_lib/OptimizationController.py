from ibl_lib import IBLOptimizer, IBLEpics, utils, log

class OptimizationController:
    def __init__(self):
        self.opt1 = IBLOptimizer(epsilon=0, alpha=1e-2, eta=0.8, gamma = 32, reduce_space=True, 
                 start_component='BC11', target_component='BC31', components='ibl-components.csv', 
                 madx_call_file='ibl.madx', madx_mag_file='ibl_mag_set.txt', madx_directory='madx_ibl',
                 space_file='opt_bc31')
        
        self.opt2 = IBLOptimizer(epsilon=0, alpha=1e-2, eta=0.8, gamma = 32, reduce_space=True, 
                 start_component='Q2X', target_component='BC81', components='ibl-components.csv', 
                 madx_call_file='ibl.madx', madx_mag_file='ibl_mag_set.txt', madx_directory='madx_ibl',
                 space_file='opt_bc81')
        
        self.opt2 = IBLOptimizer(epsilon=0, alpha=1e-2, eta=0.8, gamma = 32, reduce_space=True, 
                 start_component='Q2X', target_component='BC81', components='ibl-components.csv', 
                 madx_call_file='ibl.madx', madx_mag_file='ibl_mag_set.txt', madx_directory='madx_ibl',
                 space_file='opt_tum')
        
        self.ibl = IBLEpics(components='ibl-components.csv', init_target=None)
    
