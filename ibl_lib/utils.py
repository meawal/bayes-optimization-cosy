import subprocess
import sys
import importlib
from contextlib import contextmanager

class utils:
    PREFIX = 'SIM:'
    
    def import_module(package, pip_name=None):
        '''Imports and returns a module. If the module does not exist then it gets installed
        through pip first. 
        
        Args:
        -----
            package: str. The name of the package.
            pip_name: str (optional). The pip name of the package if it is different from the 
                package name
        '''
        if pip_name is None:
            pip_name = package
        try:
            module = importlib.import_module(package)
        except ModuleNotFoundError as e:
            from ibl_lib import log
            log.warning(str(e))
            log.info(f'installing missing module "{package}"')
            subprocess.check_call([sys.executable, "-m", "pip", "install", pip_name])
            module = importlib.import_module(package)
        return module
        

    # define a method to redirect stderr from Mad-X
    _wurlizter = import_module('wurlitzer')
    std_redirect = _wurlizter.pipes
    
    # declare fibonacci series for waiting on failed calls to epics
    fibonacci = [.1,.1,.2,.3,.5,.8]
    
    # based on https://eli.thegreenplace.net/2015/redirecting-all-kinds-of-stdout-in-python/
    # with changes to stderr
    @contextmanager
    def c_stderr_redirect(stream):
        import ctypes
        import io
        import os, sys
        import tempfile
        libc = ctypes.CDLL(None)
        c_stdout = ctypes.c_void_p.in_dll(libc, 'stderr')
        # The original fd stdout points to. Usually 1 on POSIX systems.
        original_stdout_fd = sys.stderr.fileno()
        def _redirect_stdout(to_fd):
            """Redirect stdout to the given file descriptor."""
            # Flush the C-level buffer stdout
            libc.fflush(c_stdout)
            # Flush and close sys.stdout - also closes the file descriptor (fd)
            sys.stderr.close()
            # Make original_stdout_fd point to the same file as to_fd
            os.dup2(to_fd, original_stdout_fd)
            # Create a new sys.stdout that points to the redirected fd
            sys.stderr = io.TextIOWrapper(os.fdopen(original_stdout_fd, 'wb'))
        # Save a copy of the original stdout fd in saved_stdout_fd
        saved_stdout_fd = os.dup(original_stdout_fd)
        try:
            # Create a temporary file and redirect stdout to it
            tfile = tempfile.TemporaryFile(mode='w+b')
            _redirect_stdout(tfile.fileno())
            # Yield to caller, then redirect stdout back to the saved fd
            yield
            _redirect_stdout(saved_stdout_fd)
            # Copy contents of temporary file to the given stream
            tfile.flush()
            tfile.seek(0, io.SEEK_SET)
            stream.write(tfile.read())
        finally:
            tfile.close()
            os.close(saved_stdout_fd)
            
utils.import_module('pandas')
utils.import_module('numpy')
utils.import_module('epics', pip_name='pyepics')
utils.import_module('cpymad')
utils.import_module('wurlitzer')
utils.import_module('scipy')
utils.import_module('matplotlib')
utils.import_module('sklearn')
