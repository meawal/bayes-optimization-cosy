import time
def deco(func):
    def wrapper(*args, **kwargs):
        print('in deco')
        ret = func(*args, **kwargs)
        print('after exe.. sleeping')
        time.sleep(2)
        print('done')
        return ret
    return wrapper