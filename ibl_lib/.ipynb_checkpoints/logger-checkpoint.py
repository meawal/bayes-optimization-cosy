import logging
import datetime as dt
import os

# define a filter to seperate different logging leverls
class LogFilter(logging.Filter):
    def __init__(self, level, exclude=False):
        self.level = level
        self.exclude = exclude

    def filter(self, record):
        if self.exclude:
            return record.levelno != self.level
        else:
            return record.levelno == self.level
        
# create a direcotry for the log files
if not os.path.exists('log'):
    os.mkdir('log')
    
# define log files' name
now=dt.datetime.now()
debug_file = now.strftime('log/debug_%Y-%m-%d_%H%M.log')
info_file = now.strftime('log/info_%Y-%m-%d_%H%M.log')

# define a log instance
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

# debuging log data
file_handler = logging.FileHandler(debug_file, mode='w')
formatter    = logging.Formatter('%(asctime)s:: %(message)s')
file_handler.setFormatter(formatter)
file_handler.addFilter(LogFilter(logging.DEBUG, exclude=False))
log.addHandler(file_handler)

# info+ log data
file_handler = logging.FileHandler(info_file, mode='w')
formatter    = logging.Formatter('%(asctime)s-%(levelname)s:: %(message)s')
file_handler.setFormatter(formatter)
file_handler.addFilter(LogFilter(logging.DEBUG, exclude=True))
log.addHandler(file_handler)

