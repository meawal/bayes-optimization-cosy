import pandas as pd

class IBL:
    def __init__(self, file='ibl-components.csv'):
        self.components = pd.read_csv(file)
        self.magnets = self.components[(self.components.trainable == True) & 
                self.components.type.isin(['quadrupole', 'h_steerer', 'v_steerer'])]
        self.beamcups = self.components[self.components.type.isin(['beam_cup', 'strip_foil'])]
