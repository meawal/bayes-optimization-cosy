import numpy as np
import pandas as pd
from collections import OrderedDict
from ibl_lib import log

class IBL:
    def __init__(self, components='ibl-components.csv'):
        self.components = pd.read_csv(components)
        self.components.set_index('name', inplace=True)
        self.components.sort_values('s', inplace=True)
        log.info(f'file {components} is loaded')
        
        self._magnets = None
        self._beamcups = None
        self._target_comp = None
        
        self.first_bc = self.beamcups.iloc[0]
        self.init_target = None
        
    def read_magnets(self):
        raise NotImplementedError
        
    def set_magnets(self, value_dict):
        raise NotImplementedError
        
    def wait_next_beam(self):
        raise NotImplementedError
        
    @property
    def radius(self):
        return 36
    
    @property
    def ex(self):
        return 5.68
    
    @property
    def ey(self):
        return 10.40
    
    @property
    def is_magnets_ready(self):
        raise NotImplementedError
        
    @property
    def efficiency(self):
        raise NotImplementedError
        
    @property
    def value_bounds(self):
        raise NotImplementedError
        
    @property
    def magnets(self):
        if self._magnets is None:
            # declare magnets
            self._magnets = self.components[
                (self.components.trainable == True) &
                self.components.type.isin(['quadrupole', 'h_steerer', 'v_steerer'])
            ].drop('s', axis=1).copy()
            self._magnets['value'] = None
        return self._magnets
    
    @property
    def beamcups(self):
        if self._beamcups is None:
            # declare beamcups
            self._beamcups = self.components[self.components.type.isin(
                ['beam_cup', 'strip_foil'])].copy()
        return self._beamcups
    
    @property
    def target_comp(self):
        if self._target_comp is None:
            self._target_comp = self.beamcups[self.beamcups.s == self.beamcups.s.max()].iloc[0]
            self._target_comp['value'] = None
        return self._target_comp
