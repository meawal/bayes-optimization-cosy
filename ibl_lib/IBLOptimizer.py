import numpy as np
from ibl_lib import log, StepBayesianOptimizer, IBLMadX, IBLSection
import threading, time

class IBLOptimizer:
    def __init__(self, acq='ei', epsilon=0, decay=1, alpha=1e-6, eta=0.95, gamma = 8, reduce_space=True, 
                 start_component='BC11', target_component='TUM', components='ibl-components.csv', 
                 madx_call_file='ibl.madx', madx_mag_file='ibl_mag_set.txt', madx_directory='madx_ibl',
                 space_file=None):
        self.ibl = IBLSection(start_component=start_component, target_component=target_component, 
                              components=components)
        self.alg = StepBayesianOptimizer(self.ibl.value_bounds, acq=acq, epsilon=epsilon, decay=decay, 
                                         alpha=alpha, eta=eta, gamma=gamma, reduce_space=reduce_space)
        self.madx = IBLMadX(call_file=madx_call_file, mag_file=madx_mag_file, ptc_file = None,
                            directory=madx_directory)
        
        if space_file:
            self.space_file = space_file + ('.npz' if not space_file.endswith('.npz') else '')
            try:
                space = np.load(self.space_file)['params']
                self.alg._space._params = space
                self.alg._space._target = np.zeros(space.shape[0])
                log.info(f'file {self.space_file} is loaded with {space.shape[0]} points')
            except FileNotFoundError as e:
                log.info('no saved space found.')
        
        self.kappa = 20
        self._thread = None
        self.averaging_cycles = 4
        self.next_beam_skips = 1
        
        self.__started = False
        self.__paused = False
        
        self._count_opt = 0
        self._count_mgnt = 0
        self._count_opt_total = 0
        self._count_mgnt_total = 0
                
    @property
    def summary(self):
        return f'''        Start component: {self.ibl.components.iloc[0].name}
        Target cup:\t {self.ibl.beamcups.iloc[-1].name}
        Initial target:\t {self.ibl.init_target}
        Max value:\t {self.alg.max['target'] if self.alg.max else None}
        Conv. limit:\t {self.alg.convergence_bound_limit}
        R:\t\t {self.ibl.magnets.shape[0]}
        Space reduction: {self.alg.reduce_space}
        epsilon:\t {self.alg.acq.xi}
        alpha:\t\t {self.alg._gp.alpha}
        eta:\t\t {self.alg._bounds_transformer.eta}
        gamma:\t\t {self.alg._bounds_transformer.gamma_osc}
        kappa:\t\t {self.kappa}
        Averaging cyc:\t {self.averaging_cycles}
        Next beam skips: {self.next_beam_skips}'''
        
    def start(self):
        if not self.__started or (self._thread.is_alive()==False):
            self.__started = True
            self.__paused = False
            self._thread = threading.Thread(target=self.__optimize, name='optimizer_thread')
            self._thread.daemon = True
            self._thread.start()
            log.info('starting optimization')
            log.debug(self.summary)
        else:
            log.debug('optimizer is already working. no action taken')
            
        
    def stop(self):
        log.info('stopping the optimization')
        self.__started = False
        
    def pause(self):
        log.info('pausing the optimization')
        self.__paused = True
        
    def resume(self):
        log.info('resuming the optimization')
        self.__paused = False
        
    def reset(self, convergence_bound_limit=None):
        if convergence_bound_limit:
            self.alg.convergence_bound_limit = convergence_bound_limit
        self.alg.reset_space_bound()
    
    def save_space(self):
        if self.space_file:
            space = self.alg._space._params[self.alg._space._target<=0]
            np.savez_compressed(self.space_file, params = space)
            log.debug(f'feature space saved with shape {space.shape}')

    def save_opt(self, path=None):
        import pickle as pkl
        import datetime as dt
        if path is None:
            now = dt.datetime.now()
            path = now.strftime("opt_%Y-%m-%d_%H%m.pkl")
        elif not path.endswith('.pkl'):
            path += '.pkl'
        with open(path, 'wb') as f:
            pkl.dump(self.alg._space, f)

    def load_opt(self, path):
        import pickle as pkl
        with open(path, 'rb') as f:
            self.alg._space = pkl.load(f)
        
    def restore_best_settings(self):
        self.ibl.set_magnets(self.alg.max['params'])
        
    def calc_target_variance(self, s=20):
        values = []
        log.debug(f'calculating variance over {s} samples')
        for i in range(s):
            values.append(self.ibl.read_target_value())
            self.ibl.wait_next_beam(skip=0)
            log.debug(f'\t{i}: {values[-1]:.1f}')
        log.info(f'variance = {np.std(values):.1f}')
        log.info(f'as a factor of mean = {np.std(values)/np.mean(values):.1f}')
        
    @property
    def is_alive(self):
        return self._thread.is_alive()
    
    @property
    def is_paused(self):
        return self.__paused
    
    def validate_madx(self, features):
        madx_magnets = {}
        for key in features:
            magnet = self.ibl.magnets.loc[key]
            madx_magnets[magnet.madx] = features[key]/100 * magnet.limit

        self.madx.magnets_state = madx_magnets
        self.madx.run()

        if self.madx.twiss is None:
            log.debug('Features resulted in TWISS failur')
            return False

        ibl_section = self.madx.twiss[self.madx.twiss.s.between(
            self.ibl.start_s-.001, self.ibl.end_s+.001)]

        fx = np.sqrt(self.ibl.ex * ibl_section.betx.max())/self.ibl.radius
        fy = np.sqrt(self.ibl.ey * ibl_section.bety.max())/self.ibl.radius
        log.debug(f'betx factor = {fx:.1f}, bety factor = {fy:.1f}')

        return fx,fy
    
    def __optimize(self):
        # add the first point to the optimizer
        # read magnets values
        self.ibl.read_magnets()
        features = self.ibl.magnets.value.to_dict()

        # validate the initial features by madx simulation
        fx,fy = self.validate_madx(features)
        if (fx > self.kappa) or (fy > self.kappa):
            # the objective function in this case is based on madx
            target = 0 # -fx-fy
        else:
            # calculate objective value (BC current)
            target = self.ibl.read_target_value(self.averaging_cycles)
        try:
            # add the point to the optimizer
            log.info(f'initial target is {target}')
            self.alg.register(features, target)
        except KeyError as e:
            log.info('Initial settings are already existing in the modeling space')
            
        # log progress
        eff = f', eff. = {target/self.ibl.init_target*100:.1f}%' if self.ibl.init_target else ''
        log.debug(f'Target BC value = {target:,.2f}{eff}')
            
        self._count_opt = 0
        self._count_mgnt = 0
        # optimization loop
        while self.__started:
            self._count_opt+=1
            self._count_opt_total+=1
            log.debug(f'STEP = {self._count_opt}, space max = {self.alg.max_space_bound:.2f}')
            # while opt is paused, sleep
            while self.__paused:
                time.sleep(.1)
                
            # get next parameters by the aquisition function
            features, converged = self.alg.suggest()
            
            # if BO has converged, stop
            if converged:
                log.info('BO has converged')
                break
            
            # validate the suggested features by madx simulation
            fx,fy = self.validate_madx(features)
            if (fx > self.kappa) or (fy > self.kappa):
                # add the invalid feature point to the space of BO
                self.alg.register(features, 0)# -fx-fy)
                #log.debug(f'skipped point with value {-fx-fy}')
                # move to next feature
                continue

            # count the optimization steps reflected at the magnets
            self._count_mgnt +=1
            self._count_mgnt_total +=1
            # set the magnets and wait for the next beam cycle
            self.ibl.set_magnets(features)
            self.ibl.wait_next_beam(skip=self.next_beam_skips)
            # read the actual features from the magnets
            self.ibl.read_magnets()
            features = self.ibl.magnets.value.to_dict()
            # calculate the objective value
            target = self.ibl.read_target_value(self.averaging_cycles)
            
            # log progress
            eff = f', eff. = {target/self.ibl.init_target*100:.1f}%' if self.ibl.init_target else ''
            txt = f'Cyc. {self._count_mgnt}, target BC value = {target:,.2f}{eff}'
            if target > self.alg.max['target']:
                txt = '*NEW MAX* ' + txt
                log.debug(txt)
                log.info(txt)
            else:
                log.debug(txt)
            
            # add the observation to BO model
            try:
                self.alg.register(features, target)
            except KeyError as e:
                log.warning(f'Optimization step with duplicate point. Error: {str(e)}')
        log.info(f'optimization stopped')
        log.debug(self.summary)
