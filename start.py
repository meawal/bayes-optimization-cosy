from ibl_lib import IBLOptimizer, IBLEpics, IBLSection, utils, log
log.info(f'running BO')

ibl = IBLEpics(components='ibl-components.csv')
bc11 = IBLSection(target_component='BC11')
bc31 = IBLSection(target_component='BC31')
bc81 = IBLSection(target_component='BC81')

utils.PREFIX = ''

opt = IBLOptimizer(acq='ei', epsilon=0, decay=1, alpha=1e-4, eta=0.95, gamma = 8, reduce_space=True, 
                 start_component='BC11', target_component='TUM', components='ibl-components.csv', 
                 madx_call_file='ibl.madx', madx_mag_file='ibl_mag_set.txt', madx_directory='madx_ibl',
                 space_file='opt_qfull')

opt.ibl.init_target = None
