!***************************
!*    ____                 *
!*   / ___)__  _____   __  *
!*  | |  / _ \/ __\ \ / /  *
!*  | | | (_) \__ \\ V /   *
!*  | |__\___/|___/_/_/    *
!*   \________________)    *
!*                         *
!***************************

   IBL:sequence, refer=centre, l=l_IBL;
   endsequence;

   seqedit, sequence=IBL;
      install, element=BE23Inj, at=93.23834000000;
      install, element=DI82, at=89.02035000000;
      install, element=DI81, at=86.24035000000;
      install, element=DI71, at=70.96405000000;
      install, element=DI72, at=74.78783000000;
      install, element=DI73, at=78.61161000000;
      install, element=DI74, at=82.43539000000;
      install, element=DI31, at=24.17196000000;
      install, element=DI32, at=26.77196000000;
      install, element=DI51, at=38.62096000000;
      install, element=DI52, at=41.22096000000;
      install, element=DI11, at=2.39629000000;
      install, element=DI12, at=5.86167000000;
      install, element=QI84, at=91.09035000000;
      install, element=QI83, at=90.09035000000;
      install, element=QI82, at=85.70035000000;
      install, element=QI81, at=84.78035000000;
      install, element=QI71, at=70.03136000000;
      install, element=QI72, at=71.89674000000;
      install, element=QI73, at=73.85514000000;
      install, element=QI74, at=75.72052000000;
      install, element=QI75, at=77.67892000000;
      install, element=QI76, at=79.54430000000;
      install, element=QI77, at=81.50270000000;
      install, element=QI78, at=83.36808000000;
      install, element=QI61, at=44.17916000000;
      install, element=QI62, at=47.49556000000;
      install, element=QI63, at=50.81196000000;
      install, element=QI64, at=54.12836000000;
      install, element=QI65, at=57.44476000000;
      install, element=QI66, at=60.76116000000;
      install, element=QI67, at=64.07756000000;
      install, element=QI68, at=67.39396000000;
      install, element=QI31, at=23.32196000000;
      install, element=QI32, at=25.02196000000;
      install, element=QI33, at=25.92196000000;
      install, element=QI34, at=27.62196000000;
      install, element=QI51, at=37.77096000000;
      install, element=QI52, at=39.47096000000;
      install, element=QI53, at=40.37096000000;
      install, element=QI54, at=42.07096000000;
      install, element=QI41, at=29.22806000000;
      install, element=QI42, at=31.54036000000;
      install, element=QI43, at=33.85256000000;
      install, element=QI44, at=36.16486000000;
      install, element=QI21, at=9.72281000000;
      install, element=QI22, at=13.47971000000;
      install, element=QI23, at=17.23661000000;
      install, element=QI24, at=20.99351000000;
      install, element=QI17, at=7.39436000000;
      install, element=QI16, at=6.79436000000;
      install, element=QI13, at=3.32898000000;
      install, element=QI15, at=4.92898000000;
      install, element=QI14, at=4.12898000000;
      install, element=QI12, at=1.46360000000;
      install, element=QI11, at=0.86360000000;
      install, element=SH84, at=92.01535000000;
      install, element=SC83, at=90.57035000000;
      install, element=SH83A, at=90.40035000000;
      install, element=SC82, at=88.36035000000;
      install, element=SC81, at=85.10735000000;
      install, element=SC72, at=80.10430000000;
      install, element=SC71, at=77.10892000000;
      install, element=SC63, at=68.18396000000;
      install, element=SC62, at=50.26196000000;
      install, element=SC61, at=43.42916000000;
      install, element=SC42, at=30.98036000000;
      install, element=SC41, at=28.65806000000;
      install, element=SC22, at=14.08971000000;
      install, element=SC21, at=8.59936000000;
      install, element=SC11, at=4.47898000000;
   endedit;

   seqedit, sequence=IBL;
      install, element=PGX11, at=0.00000000000;
      install, element=PGY11, at=0.00000000000;
      install, element=PGX21, at=7.84436000000;
      install, element=PGY21, at=7.84436000000;
      install, element=PGX31, at=22.87200000000;
      install, element=PGY31, at=22.87200000000;
      install, element=PGX41, at=28.07200000000;
      install, element=PGY41, at=28.07200000000;
      install, element=PGX51, at=37.32100000000;
      install, element=PGY51, at=37.32100000000;
      install, element=PGX61, at=42.52100000000;
      install, element=PGY61, at=42.52100000000;
      install, element=PGX71, at=69.05220000000;
      install, element=PGY71, at=69.05220000000;
      install, element=PGX81, at=84.34730000000;
      install, element=PGY81, at=84.34730000000;

